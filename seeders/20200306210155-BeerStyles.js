'use strict';

const path = require('path');

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up(queryInterface, Sequelize) {
    const {styles} = await import('../src/data/styleSeeds.mjs');
    await queryInterface.bulkInsert('BeerStyles', styles, {});
    await queryInterface.sequelize.query(`select setval('"BeerStyles_id_seq"', (select max(id) from public."BeerStyles"), true);`);
  },

  async down(queryInterface, Sequelize) {
    const Op = Sequelize.Op
    await queryInterface.bulkDelete('BeerStyles', {});
  }
};
