import { withInclude } from "./includes"
import { withPagination } from "./pagination"
import { withSort } from "./sort"
import { withWhere } from "./where"

export const listMW = [withInclude, withWhere, withSort, withPagination];