const fs = require('fs');
const path = require('path');

try {
    const file = fs.readFileSync(path.join(__dirname, '..', 'data', 'styles.json'));

    try {
        const parsed = JSON.parse(file.toString());
        const styles = [];
        for (const genStyle in parsed) {
            for (const substyle in parsed[genStyle]) {
                for (const style of parsed[genStyle][substyle]) {  
                    const ibu = style.IBU.split('-').map(v => Number.parseFloat(v));
                    const abvValues = style.ABW_ABV ? (style.ABW_ABV.slice(0, style.ABW_ABV.length - 1).split('(')) : null;
                    const minMaxAbv = abvValues && abvValues.length > 1 ? abvValues[1].split('-').map(v => Number.parseFloat(v)) : [0, 0];
                    styles.push({
                        id: style.id,
                        name: style.name,
                        typing: [genStyle, substyle],
                        description: `Color - ${style.color}. ${
                            style.clarity}. Malt aroma - ${style.perceived_malt_aroma
                            }. Hop aroma - ${style.perceived_hop_aroma}. ${style.perceived_bitterness
                            } bitterness. Fermentation - ${style.fermentation_characteristics}. ${
                            style.body} body.`,
                        minIBU: ibu.length > 0 ? ibu[0] : 0,
                        maxIBU: ibu.length > 1 ? ibu[1] : (ibu.length > 0 ? ibu[0] : 0),
                        minABV: minMaxAbv.length > 0 ? minMaxAbv[0] : 0,
                        maxABV: minMaxAbv.length > 1 ? minMaxAbv[1] : (minMaxAbv.length > 0 ? minMaxAbv[0] : 0)
                    });
                }
            }
        }
        const result = fs.writeFileSync(path.join(__dirname, '..', 'data', 'styleSeeds.mjs'), Buffer.from(JSON.stringify(styles)));
    } catch(err) {
        console.log('error parsing', err);
    }
} catch (err) {
    console.log(err);
}