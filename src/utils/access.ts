export const routesAccess = [
    {path: '/auth/login', method: 'POST', roles: ['GUEST', 'USER', 'ADMIN']},
    {path: '/auth/logout', method: 'POST', roles: ['GUEST', 'USER', 'ADMIN']},
    {path: '/auth/refresh', method: 'POST', roles: ['USER', 'ADMIN']},
    {path: '/auth/signup', method: 'POST', roles: ['GUEST', 'USER', 'ADMIN']},
    
    {path: '/file/uploadTestedImage', method: 'POST', roles: ['USER', 'ADMIN']},
    {path: '/file/uploadBreweryLogo', method: 'POST', roles: ['USER', 'ADMIN']},
    {path: '/file/upload/user-image', method: 'POST', roles: ['USER', 'ADMIN']},
    {path: '/file/private-file/*', method: 'GET', roles: ['GUEST', 'USER', 'ADMIN']},
    {path: '/status', method: 'GET', roles: ['GUEST', 'USER', 'ADMIN']},
    
    {path: /entity\/BeerStyle\/list/, method: 'GET', roles: ['GUEST', 'USER', 'ADMIN']},
    {path: /entity\/BeerStyle\/*/, method: 'GET', roles: ['GUEST', 'USER', 'ADMIN']},
    {path: /entity\/BeerStyle\/*/, method: 'DELETE', roles: ['ADMIN']},
    {path: /entity\/BeerStyle/, method: 'POST', roles: ['USER', 'ADMIN']},
    {path: /entity\/BeerStyle\/*/, method: 'PATCH', roles: ['ADMIN']},

    {path: /entity\/brewery\/list/i, method: 'GET', roles: ['GUEST', 'USER', 'ADMIN']},
    {path: /entity\/brewery\/.*/i, method: 'GET', roles: ['GUEST', 'USER', 'ADMIN']},
    {path: /entity\/brewery\/*/i, method: 'DELETE', roles: ['ADMIN']},
    {path: /entity\/brewery/i, method: 'POST', roles: ['USER', 'ADMIN']},
    {path: /entity\/brewery\/*/i, method: 'PATCH', roles: ['ADMIN']},

    {path: /entity\/user\/*/i, method: 'PATCH', roles: ['USER', 'ADMIN']},

    {path: /beer\/tested\/list/, method: 'GET', roles: ['GUEST', 'USER', 'ADMIN']},
    {path: /beer\/tested\/my\/list/, method: 'GET', roles: ['USER', 'ADMIN']},
    {path: /beer\/tested\/user\/list\/*/, method: 'GET', roles: ['GUEST', 'USER', 'ADMIN']},
    {path: '/beer/tested/*', method: 'GET', roles: ['GUEST', 'USER', 'ADMIN']},
    {path: /beer\/tested\/*/, method: 'DELETE', roles: ['USER', 'ADMIN']},
    {path: /beer\/tested/, method: 'POST', roles: ['USER', 'ADMIN']},
    {path: /beer\/tested\/*/, method: 'PATCH', roles: ['USER', 'ADMIN']},

]