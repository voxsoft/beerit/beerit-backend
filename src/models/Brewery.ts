import {
    AutoIncrement,
    BelongsTo,
    Column,
    ForeignKey,
    Model,
    PrimaryKey,
    Table,
} from 'sequelize-typescript';
import { FileReference } from './FileReference';
import { User } from './User';
  
export interface BreweryInterface {
    id: number;
    name: string;
    country: string;
    moderated: boolean;
    logoId: number;
    logo?: FileReference;
}
@Table({timestamps: false})
export class Brewery extends Model<Brewery> {
    @PrimaryKey
    @AutoIncrement
    @Column
    id: number;
  
    @Column
    name: string;

    @Column
    country: string;

    @Column
    moderated: boolean;

    @ForeignKey(() => FileReference)
    @Column
    logoId: number;

    @BelongsTo(() => FileReference)
    logo: FileReference;
    
    @ForeignKey(() => User)
    @Column
    userId: number;

    @BelongsTo(() => User)
    user: User;
}