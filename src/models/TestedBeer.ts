import {
    AutoIncrement,
    Column,
    Model,
    PrimaryKey,
    Table,
    DataType,
    ForeignKey,
    BelongsTo,
    Default,
    HasMany,
    HasOne,
} from 'sequelize-typescript';
import { BeerStyle, BeerStyleInterface } from './BeerStyle';
import { Brewery } from './Brewery';
import { FileReference } from './FileReference';
import { User, UserInterface } from './User';
  
export interface TestedBeerInterface {
    id: number;
    name: string;
    abv: number;
    ibu: number;
    rating: number;
    images: string[];
    description: string;
    styleId?: number;
    style: BeerStyleInterface;
    breweryId?: number;
    brewery?: Brewery;
    createdAt: any;
    updatedAt: any;
    userId: number;
    user?: UserInterface
}

@Table
export class TestedBeer extends Model<TestedBeer> {
    @PrimaryKey
    @AutoIncrement
    @Column
    id: number;
  
    @Column
    name: string;

    @Column({type: DataType.FLOAT})
    abv: number;

    @Column({type: DataType.FLOAT})
    ibu: number;
  
    @Default(0)
    @Column({type: DataType.INTEGER})
    rating: number;

    @ForeignKey(() => FileReference)
    @Column
    imageId: number;

    @BelongsTo(() => FileReference)
    image: FileReference;

    @Column({type: DataType.TEXT})
    description: string;

    @ForeignKey(() => BeerStyle)
    @Column
    styleId: number;
  
    @BelongsTo(() => BeerStyle)
    style: BeerStyle;
  
    @ForeignKey(() => Brewery)
    @Column
    breweryId: number;
  
    @BelongsTo(() => Brewery)
    brewery: Brewery;
  
    @Column({type: DataType.DATE})
    createdAt: any;

    @Column({type: DataType.DATE})
    updatedAt: any;

    @ForeignKey(() => User)
    @Column
    userId: number;

    @BelongsTo(() => User)
    user: User;

    // @HasMany(() => Card)
    // cards: Card;
  
    // @BeforeCreate
    // static async create() {
    //   try {
    //   } catch (error) {
    //     console.log('Error', error.message);
    //   }
    // }
  
    // @BeforeUpdate
    // static async update() {
    //   try {
    //     }
    //   } catch (error) {}
    // }
  
    // async generate() {
    //   try {
    //     await this.save();
    //   } catch (error) {
    //     console.error('error: ', error.message);
    //     throw new Error('Error');
    //   }
    // }
  
    // async sendLink() {
    //   try {
    //     await this.save();
    //   } catch (error) {
    //     console.error('error: ', error.message);
    //     throw new Error('Error');
    //   }
    // }
}