'use strict';

const { DataType } = require('sequelize-typescript');

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up (queryInterface, Sequelize) {
    await queryInterface.addColumn('Breweries', 'logoId', {
      type: Sequelize.INTEGER,
      references: {
        model: 'FileReferences',
        key: 'id',
      }
    })
  },

  async down (queryInterface, Sequelize) {
    return queryInterface.removeColumn(
      'Breweries',
      'logoId'
    );
  }
};
