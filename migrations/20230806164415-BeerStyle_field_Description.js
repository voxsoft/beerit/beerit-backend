'use strict';

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up (queryInterface, Sequelize) {
    await queryInterface.addColumn('BeerStyles', 'description', {
      type: Sequelize.TEXT
    })
  },

  async down (queryInterface, Sequelize) {
    return queryInterface.removeColumn(
      'BeerStyles',
      'description'
    );
  }
};
