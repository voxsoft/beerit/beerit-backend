'use strict';

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up (queryInterface, Sequelize) {
    await queryInterface.addColumn('BeerStyles', 'moderated', {
      type: Sequelize.BOOLEAN
    });

    await queryInterface.addColumn('BeerStyles', 'typing', {
      type: Sequelize.ARRAY(Sequelize.STRING)
    });
  },

  async down (queryInterface, Sequelize) {
    await queryInterface.removeColumn(
      'BeerStyles',
      'moderated'
    );

    await queryInterface.removeColumn(
      'BeerStyles',
      'typing'
    );
  }
};
